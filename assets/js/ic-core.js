(function ($) {
    "use strict";
    jQuery(document).ready(function () {
        /**
         * Toastr notifications
         * @author ITclanbd
         **/
        function toastr_notification_message(message = '', alert_type = 'success') {
            toastr.options.closeButton = true;
            toastr.options.positionClass = "toast-top-right";
            if (alert_type === 'warning') {
                toastr.warning(message);
            } else {
                toastr.success(message);
            }
        }

    });


})
(jQuery);