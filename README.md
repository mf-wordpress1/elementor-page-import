## Elementor page import from google spreadsheet/excel

Elementor page import is a WordPress plugin to import pages from google spreadcheet/excel. Page insert and automatically switch to elementor editor with some controls.

### Features
1. [PhpSpreadsheet](https://github.com/PHPOffice/PhpSpreadsheet) package use 
2. Import page to elementor editor with oneclick 

Here is the excel file download link [Elementor page import excel](https://gitlab.com/mf-wordpress1/elementor-page-import/-/blob/master/Elementor_page_import_excel.xlsx)

### Dashboard Import Option
After download this file go to `All Pages` page and you will see a `Import page` button beside the `Add New` button.
