(function ($) {
    "use strict";
    jQuery(document).ready(function () {

        // insert import button
        $(function () {
            $('.page-title-action').after('<a href="#" class="add-new-h2 page_import_btn">' + pageImportObj.btnTitle + '</a>' +
                pageImportObj.importForm);
        });

        $(window).on('click', function (e) {
            var page_import_btn = $(document).find('.page_import_btn');
            var page_import_modal = $(document).find('#page_import_modal');
            var close = page_import_modal.find('.close');

            // When the user clicks the button, open the modal 
            if (e.target === page_import_btn[0]) {
                e.preventDefault();
                page_import_modal.show();
            }

            // When the user clicks close button or anywhere outside of the modal, close it
            if (e.target === page_import_modal[0] || e.target === close[0]) {
                page_import_modal.hide();
            }
        });

        // generate codes form ajax
        $(document).on('submit', '.import_page_form', function (e) {
            e.preventDefault();
            const thisElm = $(this);

            var page_import_modal = $(document).find('#page_import_modal');
            page_import_modal.find('.warningText').remove();

            var file_data = $('#import-file').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('action', 'import_page');
            form_data.append('importPageNonce', pageImportObj.importPageNonce);

            var submitButton = $(this).find('button[type=submit]');
            $.ajax({
                url: pageImportObj.ajaxurl,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                beforeSend: function () {
                    submitButton.addClass('ic-ajax-loading');
                    submitButton.after('<h4 class="warningText">' + pageImportObj.warningText + '</h4>');
                    thisElm.css({'opacity': '0.7', 'pointer-events': 'none'});
                },
                success: function (response) {
                    var obj = JSON.parse(response);
                    if (obj.isValid) {
                        alert(obj.message);
                        location.reload();
                    } else {
                        submitButton.removeClass('ic-ajax-loading');
                        thisElm.css({'opacity': '1', 'pointer-events': 'auto'});
                        page_import_modal.find('.warningText').remove();
                        alert(obj.message);
                    }
                },
                error: function (errorThrown, status, error) {
                    alert(errorThrown.statusText);
                    location.reload();
                }
            });
        });

    });
})(jQuery);