<?php
/**
 * Plugin Name: IC Elementor page import
 * Plugin URI:  https://www.itclanbd.com
 * Description: Elementor page import from google spreadsheet.
 * Version:     1.0.0
 * Author:      ITclan BD
 * Author URI:  https://www.itclanbd.com
 * Text Domain: ic-core
 * License:     GPL-2.0+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path: /languages
 *
 * @package ic-elementor-page-import
 * @author  ITclanbd
 * @license GPL-2.0+
 * @copyright  2020, ITclan BD
 */

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 */
define('IC_CORE_VERSION', '1.0.0');

if (!function_exists('is_plugin_active')) {
    include_once(ABSPATH . 'wp-admin/includes/plugin.php');
}

/* define plugin file */
if (!defined('IC_CORE_PLUGIN_FILE')) {
    define('IC_CORE_PLUGIN_FILE', __FILE__);
}

/* define plugin path */
if (!defined('IC_CORE_BASENAME')) {
    define('IC_CORE_BASENAME', plugin_basename(__FILE__));
}

/* define plugin path */
if (!defined('IC_CORE_PATH')) {
    define('IC_CORE_PATH', plugin_dir_path(__FILE__));
}

/* define plugin URL */
if (!defined('IC_CORE_URL')) {
    define('IC_CORE_URL', plugins_url() . '/ic-elementor-page-import');
}

/* define inc URL */
if (!defined('IC_INC_URL')) {
    define('IC_INC_URL', IC_CORE_URL . '/inc');
}

/* define inc path */
if (!defined('IC_INC_DIR')) {
    define('IC_INC_DIR', IC_CORE_PATH . 'inc');
}
/*
** Load plugin class
*/
require_once(IC_INC_DIR . '/ITclanCore.php');