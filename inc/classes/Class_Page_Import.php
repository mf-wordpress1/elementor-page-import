<?php
require_once IC_INC_DIR . '/lib/vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xls;

class Class_Page_Import {

	public function __construct() {
		add_action( 'admin_enqueue_scripts', [ $this, 'ic_enqueue_admin_script' ] );
		add_action( 'wp_ajax_import_page', [ $this, 'ic_page_import_ajax' ] );
	}

	// enqueue script and styles
	public function ic_enqueue_admin_script( $hook ) {
		$current_screen = get_current_screen();
		if ( 'edit.php' == $hook && 'page' == $current_screen->post_type ) {
			wp_enqueue_style( 'ic-page-import', plugins_url( '/admin/css/ic-page-import.css', IC_CORE_PLUGIN_FILE ), [], false );
			wp_enqueue_script( 'ic-page-import', plugins_url( '/admin/js/ic-page-import.js', IC_CORE_PLUGIN_FILE ), [ 'jquery' ], false, true );
			wp_localize_script( 'ic-page-import', 'pageImportObj', [
				'ajaxurl'         => admin_url( 'admin-ajax.php' ),
				'importForm'      => $this->page_import_form(),
				'importPageNonce' => wp_create_nonce( 'importPageNonce' ),
				'btnTitle'        => __( 'Import Page', 'ic-core' ),
				'btnLoadingText'  => __( 'Importing Please Wait...', 'ic-core' ),
				'warningText'     => __( 'Please do not close this window. It may take a while', 'ic-core' ),
			] );
		}
	}

	public function page_import_form() {
		$form_html = '';
		$form_html .= '<div id="page_import_modal" class="modal"><div class="modal-content">';
		$form_html .= '<div class="modal-header"> <span class="close">&times;</span> <h2>' . __( 'Import Page From Excel', 'ic-core' ) . '</h2> </div>';

		$form_html .= '<div class="modal-body">';
		$form_html .= '<form id="import_page_form" action="#" method="post" class="import_page_form" enctype="multipart/form-data">';

		$form_html .= '<input type="hidden" name="importPage" value="1"/>';
		$form_html .= '<input type="file" name="import-file" id="import-file" accept=".xls,.xlsx" required/>';

		$form_html .= '<p class="submit">';
		$form_html .= '<button type="submit" id="submit" class="button button-primary">' . __( 'Import Page', 'ic-core' ) . '</button>';
		$form_html .= '</p>';

		$form_html .= '</form>';
		$form_html .= '</div>';
		$form_html .= '</div></div>';

		return $form_html;
	}

	// Page import ajax callback
	public function ic_page_import_ajax() {
		ini_set( 'max_execution_time', 0 );
		set_time_limit( 0 );
		$response            = [];
		$response['isValid'] = false;

		if ( ! isset( $_POST['importPageNonce'] ) || ! wp_verify_nonce( $_POST['importPageNonce'], 'importPageNonce' ) ) {
			$response['message'] = __( 'Sorry, your nonce did not verify.', 'ic-core' );
		} else {
			$allowedFileType = [
				'application/vnd.ms-excel',
				'text/xls',
				'text/xlsx',
				'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			];

			if ( in_array( $_FILES["file"]["type"], $allowedFileType ) && $_FILES["file"]["size"] > 0 ) {

				$filename    = $_FILES["file"]["tmp_name"];
				$objPHPExcel = IOFactory::load( $filename );
				$rows        = $objPHPExcel->getActiveSheet()->toArray( null, true, true, true );
				$pageInfo    = [];
				$i           = 1;
				foreach ( $rows as $key => $row ) {
					if ( $key == 1 ) {
						continue;
					}
					if ( isset( $row['A'] ) && ! empty( $row['A'] ) ) {
						if ( $rows[ $key - 1 ]['A'] == $row['A'] ) {
							$i ++;
						} else {
							$i = 1;
						}

						$pageInfo[ $this->slugify( $row['A'] ) ]['title']                     = $row['A'];
						$pageInfo[ $this->slugify( $row['A'] ) ]['sections'][ $i ]['title']   = $row['B'];
						$pageInfo[ $this->slugify( $row['A'] ) ]['sections'][ $i ]['image']   = $row['C'];
						$pageInfo[ $this->slugify( $row['A'] ) ]['sections'][ $i ]['desc']    = $row['D'];
						$pageInfo[ $this->slugify( $row['A'] ) ]['sections'][ $i ]['ratting'] = $row['E'];
						$pageInfo[ $this->slugify( $row['A'] ) ]['sections'][ $i ]['reviews'] = $row['G'];
						$pageInfo[ $this->slugify( $row['A'] ) ]['sections'][ $i ]['phone']   = $row['H'];
					}
				}

				$totalInsert    = 0;
				$totalDuplicate = 0;
				foreach ( $pageInfo as $key => $page_item ) {
					//check if page exists
					$title       = isset( $page_item['title'] ) && ! empty( $page_item['title'] ) ? sanitize_text_field( $page_item['title'] ) : '';
					$isPageExist = get_page_by_title( $title, 'OBJECT', 'page' );
					if ( ! isset( $isPageExist ) ) {
						$args = [
							'post_type'    => 'page',
							'post_status'  => 'publish',
							'post_author'  => get_current_user_id(),
							'post_title'   => wp_strip_all_tags( $page_item['title'] ),
							'post_content' => '',
						];

						$page_id = wp_insert_post( $args );

						if ( defined( 'ELEMENTOR_VERSION' ) ) {
							update_post_meta( $page_id, '_elementor_version', ELEMENTOR_VERSION );
						}
						if ( defined( 'ELEMENTOR_PRO_VERSION' ) ) {
							update_post_meta( $page_id, '_elementor_pro_version', ELEMENTOR_PRO_VERSION );
						}

						update_post_meta( $page_id, '_wp_page_template', 'elementor_header_footer' );
						update_post_meta( $page_id, '_elementor_edit_mode', 'builder' );
						update_post_meta( $page_id, '_elementor_template_type', 'wp-page' );
						global $wpdb;
						$dbPrefix = $wpdb->prefix;
						$elementor_assets = new IC_Elementor_Assets();
						$sql              = $wpdb->prepare(
							'INSERT INTO `'.$dbPrefix.'postmeta` (`post_id`, `meta_key`, `meta_value`) VALUES (%d, %s, %s)',
							$page_id, '_elementor_data', $elementor_assets->ic_elementor_data( $page_item )
						);
						$wpdb->query( $sql );

						if ( $page_id ) {
							$totalInsert ++;
						}
					} else {
						$totalDuplicate ++;
					}
					$response['message'] = sprintf(
						'%1$s %2$s %3$s %4$s',
						__( 'Total page import:', 'ic-core' ),
						$totalInsert,
						__( 'Total page skip:', 'ic-core' ),
						$totalDuplicate
					);
					$response['isValid'] = true;
				}
			} else {
				$response['message'] = __( 'Invalid File Type. Upload Excel File.', 'ic-core' );
			}
		}

		echo json_encode( $response );
		die();
	}

	public function generate_post_thumbnail( $image_url, $post_id ) {
		$upload_dir = wp_upload_dir();
		$image_data = file_get_contents( $image_url );
		$filename   = basename( $image_url );
		if ( wp_mkdir_p( $upload_dir['path'] ) ) {
			$file = $upload_dir['path'] . '/' . $filename;
		} else {
			$file = $upload_dir['basedir'] . '/' . $filename;
		}
		file_put_contents( $file, $image_data );

		$wp_filetype = wp_check_filetype( $filename, null );
		$attachment  = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_title'     => sanitize_file_name( $filename ),
			'post_content'   => '',
			'post_status'    => 'inherit'
		);
		$attach_id   = wp_insert_attachment( $attachment, $file, $post_id );
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
		$res1        = wp_update_attachment_metadata( $attach_id, $attach_data );
		$res2        = set_post_thumbnail( $post_id, $attach_id );
	}

	public function insert_tax( $taxonomy, $toImport ) {
		if ( isset( $toImport ) && ! empty( $toImport ) ) {
			$taxArr = [];
			$terms  = explode( ',', sanitize_text_field( $toImport ) );
			foreach ( $terms as $term ) {
				// Check if the term exists
				$tax = term_exists( $term, $taxonomy, 0 );
				if ( ! $tax ) {
					$tax = wp_insert_term( $term, $taxonomy, array( 'parent' => 0 ) );
				}
				array_push( $taxArr, $tax['term_taxonomy_id'] );
			}

			return $taxArr;
		}
	}

	public function slugify( $text, $divider = '-' ) {
		// replace non letter or digits by divider
		$text = preg_replace( '~[^\pL\d]+~u', $divider, $text );

		// transliterate
		$text = iconv( 'utf-8', 'us-ascii//TRANSLIT', $text );

		// remove unwanted characters
		$text = preg_replace( '~[^-\w]+~', '', $text );

		// trim
		$text = trim( $text, $divider );

		// remove duplicate divider
		$text = preg_replace( '~-+~', $divider, $text );

		// lowercase
		$text = strtolower( $text );

		if ( empty( $text ) ) {
			return 'n-a';
		}

		return $text;
	}
}

new Class_Page_Import();