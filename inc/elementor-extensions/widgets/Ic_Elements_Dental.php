<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class Ic_Elements_Dental extends Widget_Base {

	public function get_name() {
		return 'ic-dental';
	}

	public function get_title() {
		return __( 'Dental', 'ic-core' );
	}

	public function get_icon() {
		return 'ic eicon-wordpress';
	}

	public function get_categories() {
		return [ 'ic_elements' ];
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ic-core' ),
			]
		);

		$this->add_control(
			'title', [
				'label'       => __( 'Title of Dental Practice', 'ic-core' ),
				'type'        => Controls_Manager::TEXT,
				'label_block' => true,
			]
		);

		$this->add_control(
			'image',
			array(
				'label'   => esc_html__( 'Image of Business', 'ic-core' ),
				'type'    => Controls_Manager::MEDIA,
				'dynamic' => array( 'active' => true ),
			)
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'    => 'image-size',
				'exclude' => [],
				'include' => [],
				'default' => 'full',
			]
		);

		$this->add_control(
			'desc', [
				'label'       => __( 'Description', 'ic-core' ),
				'type'        => Controls_Manager::WYSIWYG,
				'dynamic'     => [
					'active' => true,
				],
				'placeholder' => __( 'Type your description here', 'ic-core' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'ratting',
			[
				'label'   => __( 'Google Rating', 'ic-core' ),
				'type'    => Controls_Manager::NUMBER,
				'min'     => .1,
				'max'     => 5,
				'step'    => .1,
				'default' => 5,
			]
		);

		$this->add_control(
			'search_volume',
			[
				'label' => __( 'Search Volume', 'ic-core' ),
				'type'  => Controls_Manager::TEXT,
			]
		);

		$this->add_control(
			'reviews',
			[
				'label'   => __( 'Google Reviews', 'ic-core' ),
				'type'    => Controls_Manager::NUMBER,
				'min'     => 1,
				'max'     => 90000,
				'step'    => 1,
				'default' => 5,
			]
		);

		$this->add_control(
			'phone',
			[
				'label' => __( 'Telephone', 'ic-core' ),
				'type'  => Controls_Manager::TEXT,
			]
		);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings_for_display();
		if ( $settings['title'] ):
			?>
            <section class="ic-dental-area">
                <h3 class="ic-dental-title"><?php echo esc_html( $settings['title'] ) ?></h3>
                <div class="ic-dental-image">
					<?php echo Group_Control_Image_Size::get_attachment_image_html( $settings, 'image-size', 'image' ); ?>
                </div>
                <div class="ic-dental-desc">
					<?php echo wp_kses_post( $settings['desc'] ) ?>
                </div>
                <div class="elementor-star-rating">
                    <i class="elementor-star-full"></i>
                    <i class="elementor-star-full"></i>
                    <i class="elementor-star-full"></i>
                    <i class="elementor-star-full"></i>
                    <i class="elementor-star-full"></i>
                </div>
                <div class="ic-dental-review"><?php echo wp_kses_post( $settings['reviews'] ) ?></div>
                <div class="ic-dental-phone">
                    <a href="tel:<?php esc_attr_e( $settings['phone'] ); ?>"><?php echo esc_html( $settings['phone'] ); ?></a>
                </div>
            </section>
		<?php
		endif;
	}
}

//Plugin::instance()->widgets_manager->register_widget_type( new Ic_Elements_Dental() );