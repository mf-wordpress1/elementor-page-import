<?php
/**
 * Loading Elementor Widgets
 * @since 1.0.0
 * @author ITclanbd
 */
if ( ! function_exists( 'ic_elementor_elements' ) ) {
	function ic_elementor_elements() {

		foreach ( glob( plugin_dir_path( __FILE__ ) . "widgets/*.php" ) as $file ) {
			include_once $file;
		}

	}
}
add_filter( 'elementor/widgets/widgets_registered', 'ic_elementor_elements' );


/**
 * Add Widget Category
 * @since 1.0.0
 * @author ITclanbd
 */
require_once( IC_INC_DIR . '/elementor-extensions/elementor-section.php' );

/**
 * Elementor editor scripts
 */
add_action( 'elementor/editor/before_enqueue_scripts', 'enqueue_editor_scripts' );
function enqueue_editor_scripts() {
	wp_enqueue_style(
		'ic-core-elementor-editor',
		IC_CORE_URL . '/admin/css/ic-editor.css',
		null,
		IC_CORE_VERSION
	);

	wp_enqueue_script(
		'ic-core-elementor-editor',
		IC_CORE_URL . '/admin/js/ic-editor.js',
		[ 'elementor-editor', 'jquery' ],
		IC_CORE_VERSION,
		true
	);
}