<?php

class ITclanCore {

	public function __construct() {
		add_action( 'plugins_loaded', [ $this, 'ic_set_locale' ], 20 );
		add_action( 'activate_' . IC_CORE_BASENAME, [ $this, 'ic_plugin_activate' ] );
		add_action( 'deactivate_' . IC_CORE_BASENAME, [ $this, 'ic_plugin_deactivate' ] );

		$this->ic_core_load_files();
	}

	public function ic_set_locale() {
		/*
		** Load text domain
		*/
		load_plugin_textdomain( 'ic-core', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
	}

	public function ic_plugin_activate() {
		require_once( IC_INC_DIR . '/classes/IC_Plugin_Activator.php' );
		IC_Plugin_Activator::activate();
	}

	public function ic_plugin_deactivate() {
		require_once( IC_INC_DIR . '/classes/IC_Plugin_Deactivator.php' );
		IC_Plugin_Deactivator::deactivate();
	}

	public function ic_core_load_files() {

		if ( did_action( 'elementor/loaded' ) ) {
			require_once( IC_INC_DIR . '/elementor-extensions/elementor-init.php' );
		}

		if(is_admin()){
			require_once( IC_INC_DIR . '/classes/IC_Elementor_Assets.php' );
			require_once( IC_INC_DIR . '/classes/Class_Page_Import.php' );
		}

		require_once( IC_INC_DIR . '/classes/IC_Core_Functions.php' );
	}
}

new ITclanCore();
